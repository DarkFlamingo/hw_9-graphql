const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require('./generated/prisma-client');
const Query = require('./resolvers/query.js');
const Mutation = require('./resolvers/mutation.js');
const Subscription = require('./resolvers/subscription.js');
const Message = require('./resolvers/message.js');
const Answer = require('./resolvers/answer.js');

const resolvers = {
  Query,
  Mutation,
  Subscription,
  Message,
  Answer,
};

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: { prisma },
});

server.start(() => console.log('http://localhost:4000'));
