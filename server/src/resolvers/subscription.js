const newMessageSubcribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .message({
      mutation_in: ['CREATED'],
    })
    .node();
};

const newAnswerSubcribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .answer({
      mutation_in: ['CREATED'],
    })
    .node();
};

const updatedAnswerSubcribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .answer({
      mutation_in: ['UPDATED'],
    })
    .node();
};

const updatedMessageSubcribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .message({
      mutation_in: ['UPDATED'],
    })
    .node();
};

const newMessage = {
  subscribe: newMessageSubcribe,
  resolve: (payload) => payload,
};

const newAnswer = {
  subscribe: newAnswerSubcribe,
  resolve: (payload) => payload,
};

const updatedMessage = {
  subscribe: updatedMessageSubcribe,
  resolve: (payload) => payload,
};

const updatedAnswer = {
  subscribe: updatedAnswerSubcribe,
  resolve: (payload) => payload,
};

module.exports = { newMessage, newAnswer, updatedMessage, updatedAnswer };
