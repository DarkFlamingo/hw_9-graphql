const postMessage = (parent, args, context, info) => {
  return context.prisma.createMessage({
    text: args.text,
    like: 0,
    dislike: 0,
  });
};

const postAnswer = async (parent, args, context, info) => {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${messageId} does not exist`);
  }

  return context.prisma.createAnswer({
    text: args.text,
    like: 0,
    dislike: 0,
    message: { connect: { id: args.messageId } },
  });
};

const likeMessage = async (parent, args, context, info) => {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${messageId} does not exist`);
  }

  const likeCount = await context.prisma.message({ id: args.messageId }).like();

  return context.prisma.updateMessage({
    where: { id: args.messageId },
    data: {
      like: likeCount + 1,
    },
  });
};

const dislikeMessage = async (parent, args, context, info) => {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${messageId} does not exist`);
  }

  const dislikeCount = await context.prisma
    .message({ id: args.messageId })
    .dislike();

  return context.prisma.updateMessage({
    where: { id: args.messageId },
    data: {
      dislike: dislikeCount + 1,
    },
  });
};

const likeAnswer = async (parent, args, context, info) => {
  const answerExists = await context.prisma.$exists.answer({
    id: args.answerId,
  });

  if (!answerExists) {
    throw new Error(`Answer with ID ${args.answerId} does not exist`);
  }

  const likeCount = await context.prisma.answer({ id: args.answerId }).like();

  return context.prisma.updateAnswer({
    where: { id: args.answerId },
    data: {
      like: likeCount + 1,
    },
  });
};

const dislikeAnswer = async (parent, args, context, info) => {
  const answerExists = await context.prisma.$exists.answer({
    id: args.answerId,
  });

  if (!answerExists) {
    throw new Error(`Answer with ID ${args.answerId} does not exist`);
  }

  const dislikeCount = await context.prisma
    .answer({ id: args.answerId })
    .dislike();

  return context.prisma.updateAnswer({
    where: { id: args.answerId },
    data: {
      dislike: dislikeCount + 1,
    },
  });
};

module.exports = {
  postMessage,
  postAnswer,
  likeMessage,
  likeAnswer,
  dislikeMessage,
  dislikeAnswer,
};
