import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput) {
    messages(orderBy: $orderBy) {
      count
      messageList {
        id
        text
        like
        dislike
        answers {
          id
          text
          like
          dislike
        }
      }
    }
  }
`;
