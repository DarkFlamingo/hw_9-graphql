import gql from 'graphql-tag';

export const POST_ANSWER_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $text: String!) {
    postAnswer(messageId: $messageId, text: $text) {
      id
      text
      like
      dislike
    }
  }
`;

export const LIKE_ANSWER_MUTATION = gql`
  mutation UpdateMutation($answerId: ID!) {
    likeAnswer(answerId: $answerId) {
      id
      text
      like
      dislike
    }
  }
`;

export const DISLIKE_ANSWER_MUTATION = gql`
  mutation UpdateMutation($answerId: ID!) {
    dislikeAnswer(answerId: $answerId) {
      id
      text
      like
      dislike
    }
  }
`;
