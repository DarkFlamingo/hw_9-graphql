import gql from 'graphql-tag';

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postMessage(text: $text) {
      id
      text
      like
      dislike
      answers {
        id
        text
        like
        dislike
      }
    }
  }
`;

export const LIKE_MESSAGE_MUTATION = gql`
  mutation UpdateMutation($messageId: ID!) {
    likeMessage(messageId: $messageId) {
      id
      text
      like
      dislike
      answers {
        id
        text
        like
        dislike
      }
    }
  }
`;

export const DISLIKE_MESSAGE_MUTATION = gql`
  mutation UpdateMutation($messageId: ID!) {
    dislikeMessage(messageId: $messageId) {
      id
      text
      like
      dislike
      answers {
        id
        text
        like
        dislike
      }
    }
  }
`;
