import gql from 'graphql-tag';

export const NEW_ANSWERS_SUBSCRIPTION = gql`
  subscription {
    newAnswer {
      id
      text
      like
      dislike
      message {
        id
      }
    }
  }
`;

export const UPDATE_ANSWER_SUBSCRIPTION = gql`
  subscription {
    updatedAnswer {
      id
      text
      like
      dislike
      message {
        id
      }
    }
  }
`;
