import gql from 'graphql-tag';

export const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      like
      dislike
      answers {
        id
        text
        like
        dislike
      }
    }
  }
`;

export const UPDATE_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    updatedMessage {
      id
      text
      like
      dislike
      answers {
        id
        text
        like
        dislike
      }
    }
  }
`;
