const OrderByType = {
  like_DESC: 'like_DESC',
  like_ASC: 'like_ASC',
  dislike_DESC: 'dislike_DESC',
  dislike_ASC: 'dislike_ASC',
  createdAt_DESC: 'createdAt_DESC',
  createdAt_ASC: 'createdAt_ASC',
};

export { OrderByType };
