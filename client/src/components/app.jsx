import MessageList from './message-list/message-list';

const App = () => {
  return (
    <div className="App">
      <header className="App-header"></header>
      <MessageList />
      <footer></footer>
    </div>
  );
};

export default App;
