import './styles.css';
import * as React from 'react';
import { ButtonGroup, ToggleButton } from '../../common/common';
import { OrderByType } from '../../../common/enums/order-by.enum';

const MessageFilter = ({ changeOrderBy, orderBy }) => {
  const radios = [
    { name: 'Create', value: '1', orderBy: OrderByType.createdAt_DESC },
    { name: 'Dislike', value: '2', orderBy: OrderByType.dislike_DESC },
    { name: 'Like', value: '3', orderBy: OrderByType.like_DESC },
  ];

  const [radioValue, setRadioValue] = React.useState(
    radios.find((item) => item.orderBy === orderBy).value
  );

  return (
    <>
      <br />
      <ButtonGroup className="mb-2">
        {radios.map((radio, idx) => (
          <ToggleButton
            key={idx}
            id={`radio-${idx}`}
            type="radio"
            variant="secondary"
            name="radio"
            value={radio.value}
            checked={radioValue === radio.value}
            onChange={(e) => setRadioValue(e.currentTarget.value)}
            onClick={() => changeOrderBy(radio.orderBy)}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
      <br />
    </>
  );
};

export default MessageFilter;
