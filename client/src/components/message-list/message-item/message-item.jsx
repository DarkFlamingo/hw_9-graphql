import './styles.css';
import AnswerList from './answer-list/answer-list';
import { Button } from '../../common/common';
import { HandThumbsUp, HandThumbsDown } from 'react-bootstrap-icons';
import { Mutation } from 'react-apollo';
import {
  LIKE_MESSAGE_MUTATION,
  DISLIKE_MESSAGE_MUTATION,
  MESSAGE_QUERY,
} from '../../../common/apollo-queries/apollo-queries';

const MessageItem = ({ message, orderBy }) => {
  const _updateStoreAfterUpdatingMessage = (store, newMessage) => {
    store.readQuery({
      query: MESSAGE_QUERY,
      variables: { orderBy },
    });
  };

  return (
    <div className="message-item">
      <p>{message.id}</p>
      <p>{message.text}</p>
      <div className="like-dislike-wrapper">
        <Mutation
          mutation={LIKE_MESSAGE_MUTATION}
          variables={{ messageId: message.id }}
          update={(store, { data: { likeMessage } }) => {
            _updateStoreAfterUpdatingMessage(store, likeMessage);
          }}
        >
          {(updateMutation) => (
            <Button
              variant="outline-primary"
              className="message-like"
              onClick={updateMutation}
            >
              <HandThumbsUp />
            </Button>
          )}
        </Mutation>
        <p className="like-count">{message.like}</p>
        <Mutation
          mutation={DISLIKE_MESSAGE_MUTATION}
          variables={{ messageId: message.id }}
          update={(store, { data: { dislikeMessage } }) => {
            _updateStoreAfterUpdatingMessage(store, dislikeMessage);
          }}
        >
          {(updateMutation) => (
            <Button
              variant="outline-primary"
              className="message-dislike"
              onClick={updateMutation}
            >
              <HandThumbsDown />
            </Button>
          )}
        </Mutation>
        <p className="dislike-count">{message.dislike}</p>
      </div>
      <AnswerList
        answers={message.answers}
        messageId={message.id}
        orderBy={orderBy}
      />
    </div>
  );
};

export default MessageItem;
