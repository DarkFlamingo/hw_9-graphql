import './styles.css';
import * as React from 'react';
import { Button } from '../../../../common/common';
import { Mutation } from 'react-apollo';
import {
  MESSAGE_QUERY,
  POST_ANSWER_MUTATION,
} from '../../../../../common/apollo-queries/apollo-queries';

const AnswerForm = ({ messageId, toggleForm, orderBy }) => {
  const [text, setText] = React.useState('');

  const handleOnChange = (ev) => setText(ev.target.value);

  const _updateStoreAfterAddingAnswer = (store, newAnswer, messageId) => {
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: { orderBy },
    });
    const reviewedMessage = data.messages.messageList.find(
      (item) => item.id === messageId
    );
    reviewedMessage.answers.push(newAnswer);
    store.writeQuery({ query: MESSAGE_QUERY, data });
    toggleForm(false);
  };

  return (
    <div className="answer-form">
      <input
        type="text"
        onChange={handleOnChange}
        value={text}
        className="answer-input"
      />
      <Mutation
        mutation={POST_ANSWER_MUTATION}
        variables={{ messageId, text }}
        update={(store, { data: { postAnswer } }) => {
          _updateStoreAfterAddingAnswer(store, postAnswer, messageId);
        }}
      >
        {(postMutation) => (
          <Button
            className="message-input-button"
            variant="primary"
            onClick={postMutation}
          >
            Send
          </Button>
        )}
      </Mutation>
    </div>
  );
};

export default AnswerForm;
