import './styles.css';
import { Button } from '../../../../common/common';
import { HandThumbsUp, HandThumbsDown } from 'react-bootstrap-icons';
import { Mutation } from 'react-apollo';
import {
  LIKE_ANSWER_MUTATION,
  DISLIKE_ANSWER_MUTATION,
  MESSAGE_QUERY,
} from '../../../../../common/apollo-queries/apollo-queries';

const AnswerItem = ({ answer, orderBy }) => {
  const _updateStoreAfterUpdatingAnswer = (store, newMessage, orderBy) => {
    store.readQuery({
      query: MESSAGE_QUERY,
      variables: { orderBy },
    });
  };

  return (
    <div className="answer-item">
      <div>{answer.id}</div>
      <div>{answer.text}</div>
      <div className="like-dislike-wrapper">
        <Mutation
          mutation={LIKE_ANSWER_MUTATION}
          variables={{ answerId: answer.id }}
          update={(store, { data: { likeAnswer } }) => {
            _updateStoreAfterUpdatingAnswer(store, likeAnswer, orderBy);
          }}
        >
          {(updateMutation) => (
            <Button
              variant="outline-primary"
              className="message-like"
              onClick={updateMutation}
            >
              <HandThumbsUp />
            </Button>
          )}
        </Mutation>
        <p className="like-count">{answer.like}</p>
        <Mutation
          mutation={DISLIKE_ANSWER_MUTATION}
          variables={{ answerId: answer.id }}
          update={(store, { data: { dislikeAnswer } }) => {
            _updateStoreAfterUpdatingAnswer(store, dislikeAnswer, orderBy);
          }}
        >
          {(updateMutation) => (
            <Button
              variant="outline-primary"
              className="message-dislike"
              onClick={updateMutation}
            >
              <HandThumbsDown />
            </Button>
          )}
        </Mutation>
        <p className="dislike-count">{answer.dislike}</p>
      </div>
    </div>
  );
};

export default AnswerItem;
