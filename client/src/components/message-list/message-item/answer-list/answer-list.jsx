import './styles.css';
import * as React from 'react';
import AnswerItem from './answer-item/answer-item';
import AnswerForm from './answer-form/answer-form';

const AnswerList = ({ answers, messageId, orderBy }) => {
  const [showAnswerForm, setShowAnswerForm] = React.useState(false);

  return (
    <div className="answer-list">
      {answers.length > 0 && <span>Answers</span>}
      {answers.map((item) => (
        <AnswerItem answer={item} key={item.id} orderBy={orderBy} />
      ))}
      <button onClick={() => setShowAnswerForm(!showAnswerForm)}>
        {showAnswerForm ? 'Close answer' : 'Add answer'}
      </button>
      {showAnswerForm && (
        <AnswerForm
          messageId={messageId}
          toggleForm={setShowAnswerForm}
          orderBy={orderBy}
        />
      )}
    </div>
  );
};

export default AnswerList;
