import './styles.css';
import * as React from 'react';
import { Query } from 'react-apollo';
import MessageItem from './message-item/message-item';
import MessageForm from './message-form/message-form';
import MessageFilter from './message-filter/message-filter';
import {
  MESSAGE_QUERY,
  NEW_MESSAGES_SUBSCRIPTION,
  NEW_ANSWERS_SUBSCRIPTION,
  UPDATE_ANSWER_SUBSCRIPTION,
  UPDATE_MESSAGE_SUBSCRIPTION,
} from '../../common/apollo-queries/apollo-queries';
import { OrderByType } from '../../common/enums/enums';

const MessageList = () => {
  const [orderBy, setOrderBy] = React.useState(OrderByType.createdAt_DESC);

  const _subscribeToNewMessages = (subscribeToMore) => {
    subscribeToMore({
      document: NEW_MESSAGES_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(
          ({ id }) => id === newMessage.id
        );
        if (exists) return prev;

        return {
          ...prev,
          messages: {
            messageList: [newMessage, ...prev.messages.messageList],
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  const _subscribeToNewAnswer = (subscribeToMore) => {
    subscribeToMore({
      document: NEW_ANSWERS_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newAnswer } = subscriptionData.data;
        const exists = prev.messages.messageList.some((message) =>
          message.answers.find(({ id }) => id === newAnswer.id)
        );
        if (exists) return prev;

        const { message, ...clearAnswer } = newAnswer;

        return {
          ...prev,
          messages: {
            messageList: prev.messages.messageList.map((message) =>
              newAnswer.message.id !== message.id
                ? message
                : { ...message, answers: [...message.answers, clearAnswer] }
            ),
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  const _subscribeToUpdateAnswer = (subscribeToMore) => {
    subscribeToMore({
      document: UPDATE_ANSWER_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { updatedAnswer } = subscriptionData.data;
        const exists = prev.messages.messageList.some((message) =>
          message.answers.find(
            ({ id, like }) =>
              id === updatedAnswer.id && like === updatedAnswer.like
          )
        );
        if (exists) return prev;

        const { message, ...clearAnswer } = updatedAnswer;

        return {
          ...prev,
          messages: {
            messageList: prev.messages.messageList.map((message) =>
              updatedAnswer.message.id !== message.id ? message : clearAnswer
            ),
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  const _subscribeToUpdateMessage = (subscribeToMore) => {
    subscribeToMore({
      document: UPDATE_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { updatedMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(
          ({ id, like }) =>
            id === updatedMessage.id && like === updatedMessage.like
        );
        if (exists) return prev;

        return {
          ...prev,
          messages: {
            messageList: prev.messages.messageList.map((message) =>
              updatedMessage.id !== message.id ? message : updatedMessage
            ),
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  return (
    <Query query={MESSAGE_QUERY} variables={{ orderBy }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <div>Loading...</div>;
        if (error) return <div>Error</div>;

        _subscribeToNewMessages(subscribeToMore);
        _subscribeToNewAnswer(subscribeToMore);
        _subscribeToUpdateAnswer(subscribeToMore);
        _subscribeToUpdateMessage(subscribeToMore);

        const {
          messages: { messageList },
        } = data;

        return (
          <div>
            <MessageFilter changeOrderBy={setOrderBy} orderBy={orderBy} />
            <MessageForm orderBy={orderBy} />
            {messageList.map((item) => (
              <MessageItem key={item.id} message={item} orderBy={orderBy} />
            ))}
          </div>
        );
      }}
    </Query>
  );
};

export default MessageList;
