import './styles.css';
import * as React from 'react';
import { Button } from '../../common/common';
import { Mutation } from 'react-apollo';
import {
  POST_MESSAGE_MUTATION,
  MESSAGE_QUERY,
} from '../../../common/apollo-queries/apollo-queries';

const MessageForm = ({ orderBy }) => {
  const [text, setText] = React.useState('');

  const handleOnChange = (ev) => setText(ev.target.value);

  const _updateStoreAfterAddingMessage = (store, newMessage, orderBy) => {
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: { orderBy: orderBy },
    });
    store.writeQuery({ query: MESSAGE_QUERY, data: data });
    setText('');
  };

  return (
    <div>
      <input
        type="text"
        onChange={handleOnChange}
        value={text}
        className="edit-message-input"
      />
      <Mutation
        mutation={POST_MESSAGE_MUTATION}
        variables={{ text }}
        update={(store, { data: { postMessage } }) => {
          _updateStoreAfterAddingMessage(store, postMessage, orderBy);
        }}
      >
        {(postMutation) => (
          <Button
            className="message-input-button"
            variant="primary"
            onClick={postMutation}
          >
            Send
          </Button>
        )}
      </Mutation>
    </div>
  );
};

export default MessageForm;
